#Richard Reksoatmodjo, LLNL, 2024
#Adapted from methods originally tested by Jens von der Linden

from sys import version_info
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import numpy.linalg as nl
import MDSplus as mds
from matplotlib.ticker import EngFormatter

time_formatter = EngFormatter('s')
current_formatter = EngFormatter('A')

def make_shot(shots_by_date):
    shots = []
    for key in shots_by_date.keys():
        for shot in shots_by_date[key]:
            shots.append(int(str(key) + str(shot).zfill(3)))
    return shots

def mst_ops(shot, comp='dave', tree='mst'):
    r"""
    Load ops signals
    """
    mst_ops_node_paths = {"f": "\mst_ops::f", 
                      "ip": "\mst_ops::ip",
                      "btw": "\mst_ops::btw",
                      "btave": "\mst_ops::btave"}
    
    server = comp + '.physics.wisc.edu'
    conn = mds.Connection(server)
    try:
        conn.openTree(tree, shot)
        ref_node_path = mst_ops_node_paths[list(mst_ops_node_paths.keys())[0]]
        ref_time = np.asarray(conn.get('DIM_OF(' + ref_node_path + ')'))
        ref_time = ref_time.astype("float64")
        ops = {}
        for key in mst_ops_node_paths.keys():
            node = conn.get(mst_ops_node_paths[key])
            data = node.data()
            time = np.asarray(conn.get('DIM_OF(' + mst_ops_node_paths[key] + ')'))
            data = data.astype("float64")
            time = time.astype("float64")
            assert np.allclose(time, ref_time), "time bases differ"
            ops[key] = data
    except:
        print('Failure', shot)
    return ops, time

def build_ops_ds(shots, shot_null):
    ref_data, ref_time = mst_ops(1231024038)
    ds = xr.Dataset(
                    data_vars={
                               key: (
                                     ["shot", "time"],
                                     np.array([mst_ops(shot_null + shot)[0][key] for shot in shots]),
                                    )
                                for key in ref_data.keys()
                               },
                               coords={"time": ("time", ref_time), "shot": ("shot", shots)},
                    )
    return ds

import scipy.signal as spsig

def find_sawteeth(data, shot, prom=10, dist=100, plot=True, Bthresh=300):

    btave=data['btave'].sel(shot=shot)
    '''
    #Determine flat-top period boundary based on Bthresh value
    
    ii=0
    while btave.values[ii]<Bthresh:
        ii+=1

    t0=ii

    while btave.values[ii]>Bthresh:
        ii+=1

    tf=ii
    
    btave=btave[t0:tf]
    '''
    t_Bt = btave.coords['time']

    #Finding sawteeth signals

    peaks, properties = spsig.find_peaks(btave.values,distance=dist,prominence=(prom,None))
    valleys, properties2 = spsig.find_peaks(-1*btave.values,distance=dist,prominence=(prom,None))

    while peaks[0]<valleys[0]:
        peaks=np.delete(peaks,0)
        print('Removed first peak')

    while valleys[-1]<peaks[-1]:
        peaks=np.delete(peaks,-1)
        print('Removed last peak')
    
    sawteeth=list(zip(valleys[:-1],valleys[1:]))        
    
    #Plotting routine
    
    if plot:
    
        fig1,ax1=plt.subplots(figsize=[20,15])

        btave.plot(ax=ax1)

        ax1.plot(t_Bt[peaks],btave.values[peaks],'rx')
        #ax1.plot(t_Bt[valleys],btave.values[valleys],'kx')
        #ax1.vlines(t_Bt[peaks],ymin=btave.min(),ymax=btave.max(),colors='red',linestyles='dotted')
        ax1.vlines(t_Bt[valleys],ymin=btave.min(),ymax=btave.max(),colors='black',linestyles='dotted')

    #print('Interval from time {} to {} analyzed'.format(np.round(t_Bt[0].values,3),np.round(t_Bt[-1].values,3)))    
    print('{} sawtooth periods found'.format(len(peaks)))
    print('Peak indices: {}'.format(peaks))
    print('Valley indices: {}'.format(valleys))
    print('Peak timestamps (s): {}'.format(t_Bt[peaks].values))
    print('Valley timestamps (s): {}'.format(t_Bt[valleys].values))
    print('Sawtooth period lengths (s): {}'.format(np.diff(t_Bt[valleys].values)))
    
    return {'peaks':peaks,'valleys':valleys,'sawteeth':sawteeth}

def ops_summary_plot(mst_ds, refs=[10], signal_time = slice(0e-3, 65e-3), title='', figsize=(30, 15)):

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    
    title = title
    legend=[]

    fig, ax = plt.subplots(2, 4, figsize=figsize, sharex=True, layout='constrained')
    
    mst_ds['f'].sel(time=signal_time).plot(ax=ax[0][0],
                                           cmap='inferno', vmin=-0.5, vmax=1.1)
    mst_ds['ip'].sel(time=signal_time).plot(ax=ax[0][1], cmap='inferno')
    mst_ds['btw'].sel(time=signal_time).plot(ax=ax[0][2], cmap='inferno')
    mst_ds['btave'].sel(time=signal_time).plot(ax=ax[0][3], cmap='inferno')


    ax[0,0].set_xlabel('')
    ax[0,1].set_xlabel('')
    ax[0,2].set_xlabel('')
    ax[0,3].set_xlabel('')
    ax[0,1].set_ylabel('')
    ax[0,2].set_ylabel('')
    ax[0,3].set_ylabel('')
    ax[1,1].set_ylabel('')
    ax[1,2].set_ylabel('')
    ax[1,3].set_ylabel('')

    ax[0,0].set_title('F')
    ax[0,1].set_title(r'$I_p$')
    ax[0,2].set_title(r'$B_{t \enspace wall}$')
    ax[0,3].set_title(r'$B_{t \enspace av}$')
    
    for n, i in enumerate(refs):
        
        ax[0,0].axhline(i,color=colors[n])
        ax[0,1].axhline(i,color=colors[n])
        ax[0,2].axhline(i,color=colors[n])
        ax[0,3].axhline(i,color=colors[n])

        mst_ds['f'].sel(time=signal_time).sel(shot=i).plot(ax=ax[1][0])
        mst_ds['ip'].sel(time=signal_time).sel(shot=i).plot(ax=ax[1][1])
        mst_ds['btw'].sel(time=signal_time).sel(shot=i).plot(ax=ax[1][2])
        mst_ds['btave'].sel(time=signal_time).sel(shot=i).plot(ax=ax[1][3])
        
        legend.append(i)

    ax[1, 0].set_title('')
    ax[1, 1].set_title('')
    ax[1, 2].set_title('')
    ax[1, 3].set_title('')
    ax[1, 3].legend(legend,loc=3)    
    ax[1, 0].set_ylim(-1.0, 1.5)

    ax[0,0].xaxis.set_major_formatter(time_formatter)

    fig.suptitle(title)

    #plt.tight_layout()
    plt.show()