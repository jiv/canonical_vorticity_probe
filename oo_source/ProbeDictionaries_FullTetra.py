#Copy of ProbeDictionaries.py in Full_Tetra/
#RR, June 25 2024

MachProbeDict = {'11':{'ProbeName':'probe1_02','Bias_Voltage':'B1-2','Resistance':'RJ1-2','x': 0.530,'y': 0.000,'z': -1.470},
                 '12':{'ProbeName':'probe1_03','Bias_Voltage':'B1-3','Resistance':'RJ1-3','x': 0.350,'y': -0.180,'z': -1.192},
                 '13':{'ProbeName':'probe1_01','Bias_Voltage':'B1-1','Resistance':'RJ1-1','x': 0.170,'y': 0.000,'z': -1.470},
                 '14':{'ProbeName':'probe1_04','Bias_Voltage':'B1-4','Resistance':'RJ1-4','x': 0.350,'y': 0.180,'z': -1.192},
                 '21':{'ProbeName':'probe1_06','Bias_Voltage':'B2-2','Resistance':'RJ2-2','x': 0.000,'y': -0.530,'z': -0.975},
                 '22':{'ProbeName':'probe1_08','Bias_Voltage':'B2-4','Resistance':'RJ2-4','x': -0.180,'y': -0.350,'z': -0.697}, # flipped with 24 in Constance's connection page
                 '23':{'ProbeName':'probe1_05','Bias_Voltage':'B2-1','Resistance':'RJ2-1','x': 0.000,'y': -0.170,'z': -0.975},
                 '24':{'ProbeName':'probe1_07','Bias_Voltage':'B2-3','Resistance':'RJ2-3','x': 0.180,'y': -0.350,'z': -0.697}, # flipped with 22 in Constance's connection page
                 '31':{'ProbeName':'probe2_02','Bias_Voltage':'B3-2','Resistance':'RJ3-2','x': -0.530,'y': 0.000,'z': -1.470},
                 '32':{'ProbeName':'probe2_04','Bias_Voltage':'B3-4','Resistance':'RJ3-4','x': -0.350,'y': 0.180,'z': -1.192},
                 '33':{'ProbeName':'probe2_01','Bias_Voltage':'B3-1','Resistance':'RJ3-1','x': -0.170,'y': 0.000,'z': -1.470},
                 '34':{'ProbeName':'probe2_03','Bias_Voltage':'B3-3','Resistance':'RJ3-3','x': -0.350,'y': -0.180,'z': -1.192},
                 '41':{'ProbeName':'probe2_06','Bias_Voltage':'B4-2','Resistance':'RJ4-2','x': 0.000,'y': 0.530,'z': -0.975},
                 '42':{'ProbeName':'probe2_07','Bias_Voltage':'B4-3','Resistance':'RJ4-3','x': 0.180,'y': 0.350,'z': -0.697},
                 '43':{'ProbeName':'probe2_05','Bias_Voltage':'B4-1','Resistance':'RJ4-1','x': 0.000,'y': 0.170,'z': -0.975},
                 '44':{'ProbeName':'probe2_08','Bias_Voltage':'B4-4','Resistance':'RJ4-4','x': -0.180,'y': 0.350,'z': -0.697}}

MachProbeDict_1025 = {'11':{'ProbeName':'probe1_02','Bias_Voltage':'B1-2','Resistance':'RJ1-2'},
                 '12':{'ProbeName':'probe1_03','Bias_Voltage':'B1-3','Resistance':'RJ1-3'},
                 '13':{'ProbeName':'probe1_01','Bias_Voltage':'B1-1','Resistance':'RJ1-1'},
                 '14':{'ProbeName':'probe1_04','Bias_Voltage':'B1-4','Resistance':'RJ1-4'},
                 '21':{'ProbeName':'probe1_06','Bias_Voltage':'B2-2','Resistance':'RJ2-2'},
                 '22':{'ProbeName':'probe1_08','Bias_Voltage':'B2-4','Resistance':'RJ2-4'},
                 '23':{'ProbeName':'probe1_05','Bias_Voltage':'B2-5','Resistance':'RJ2-5'},
                 '24':{'ProbeName':'probe1_07','Bias_Voltage':'B2-3','Resistance':'RJ2-3'},
                 '31':{'ProbeName':'probe2_02','Bias_Voltage':'B3-2','Resistance':'RJ3-2'},
                 '32':{'ProbeName':'probe2_04','Bias_Voltage':'B3-4','Resistance':'RJ3-4'},
                 '33':{'ProbeName':'probe2_01','Bias_Voltage':'B3-1','Resistance':'RJ3-1'},
                 '34':{'ProbeName':'probe2_03','Bias_Voltage':'B3-3','Resistance':'RJ3-3'},
                 '41':{'ProbeName':'probe2_06','Bias_Voltage':'B4-2','Resistance':'RJ4-2'},
                 '42':{'ProbeName':'probe2_07','Bias_Voltage':'B4-5','Resistance':'RJ4-5'},
                 '43':{'ProbeName':'probe2_05','Bias_Voltage':'B4-1','Resistance':'RJ4-1'},
                 '44':{'ProbeName':'probe2_08','Bias_Voltage':'B4-4','Resistance':'RJ4-4'}}


MachProbeDict_1110 = {'11':{'ProbeName':'probe1_02','Bias_Voltage':'B1-2','Resistance':'RJ1-2'},
                 '12':{'ProbeName':'probe1_03','Bias_Voltage':'B1-3','Resistance':'RJ1-3'},
                 '13':{'ProbeName':'probe1_01','Bias_Voltage':'B1-1','Resistance':'RJ1-1'},
                 '14':{'ProbeName':'probe1_04','Bias_Voltage':'B1-4','Resistance':'RJ1-5'},
                 '21':{'ProbeName':'probe1_06','Bias_Voltage':'B2-2','Resistance':'RJ2-2'},
                 '22':{'ProbeName':'probe1_08','Bias_Voltage':'B2-4','Resistance':'RJ2-4'},
                 '23':{'ProbeName':'probe1_05','Bias_Voltage':'B2-5','Resistance':'RJ2-5'},
                 '24':{'ProbeName':'probe1_07','Bias_Voltage':'B2-3','Resistance':'RJ2-3'},
                 '31':{'ProbeName':'probe2_02','Bias_Voltage':'B3-2','Resistance':'RJ3-2'},
                 '32':{'ProbeName':'probe2_04','Bias_Voltage':'B3-4','Resistance':'RJ3-4'},
                 '33':{'ProbeName':'probe2_01','Bias_Voltage':'B3-1','Resistance':'RJ3-1'},
                 '34':{'ProbeName':'probe2_03','Bias_Voltage':'B3-3','Resistance':'RJ3-3'},
                 '41':{'ProbeName':'probe2_06','Bias_Voltage':'B4-2','Resistance':'RJ4-2'},
                 '42':{'ProbeName':'probe2_07','Bias_Voltage':'B4-5','Resistance':'RJ4-5'},
                 '43':{'ProbeName':'probe2_05','Bias_Voltage':'B4-1','Resistance':'RJ4-1'},
                 '44':{'ProbeName':'probe2_08','Bias_Voltage':'B4-4','Resistance':'RJ4-4'}}

BProbeDict = {'1B1':{'ProbeName':'probe5_01'},
              '2B1':{'ProbeName':'probe5_02'},
              '3B1':{'ProbeName':'probe5_03'},
              '4B1':{'ProbeName':'probe5_04'},
              '1B2':{'ProbeName':'probe5_05'},
              '2B2':{'ProbeName':'probe5_06'},
              '3B2':{'ProbeName':'probe5_07'},
              '4B2':{'ProbeName':'probe5_08'},
              '1BR':{'ProbeName':'probe5_09'},
              '2BR':{'ProbeName':'probe5_10'},
              '3BR':{'ProbeName':'probe5_11'},
              '4BR':{'ProbeName':'probe5_12'}}
                 