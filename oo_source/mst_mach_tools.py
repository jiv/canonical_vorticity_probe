# Karsten McCollam, UW-Madison, 2021
# Jens von der Linden canonical helicity project on MST
# Copy of mst_mach_220927.py
# Copied to oo_source and expanded by RR, June 2024

from sys import version_info
# May not work for versions before 2.7...
#if version_info.major >= 3:
#    from importlib import reload
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
from matplotlib import gridspec
import re
import numpy as np
import numpy.linalg as nl
import xarray as xr
import pandas as pd
import tqdm
import MDSplus as mds

time_formatter = EngFormatter('s')
current_formatter = EngFormatter('A')

def shot_data_Vm(shot, tip, MachProbeDict, comp='dave', tree='mst', ref_time=None):
    r'''
    '''
    probe_name = MachProbeDict[str(tip)]['ProbeName']
    server = comp + '.physics.wisc.edu'
    conn = mds.Connection(server)
    conn.openTree(tree, shot)
    signalname = '\\' + probe_name
    node = conn.get(signalname)
    data = node.data()
    time = np.asarray(conn.get('DIM_OF(' + signalname + ')'))
    data = data.astype('float64')
    time = time.astype('float64')
    if not ref_time is None:
        assert np.allclose(time, ref_time), 'time not equal'
    return data, time


def shot_data_Bdot(shot, coil, BProbeDict, comp='dave', tree='mst', ref_time=None):
    r'''
    '''
    probe_name = BProbeDict[coil]['ProbeName']
    server = comp + '.physics.wisc.edu'
    conn = mds.Connection(server)
    conn.openTree(tree, shot)
    signalname = '\\' + probe_name
    node = conn.get(signalname)
    data = node.data()
    time = np.asarray(conn.get('DIM_OF(' + signalname + ')'))
    data = data.astype('float64')
    time = time.astype('float64')
    if not ref_time is None:
        assert np.allclose(time, ref_time), 'time not equal'
    return data, time

def build_Vm_dataset(shots, shot_null, tips, MachProbeDict):
    
    ref_data, ref_time = shot_data_Vm(1231024012, 31, MachProbeDict)  
    
    ds = xr.Dataset(
                    data_vars={
                               'Vm': (
                                     ["tip","shot", "time"],
                                     np.array([[shot_data_Vm(shot_null + shot, tip, MachProbeDict, ref_time=ref_time
                                                             )[0] for shot in shots] for tip in tips])),
                               'x' : (["tip"], [MachProbeDict[str(tip)]['x'] for tip in tips]),
                               'y' : (["tip"], [MachProbeDict[str(tip)]['y'] for tip in tips]),
                               'z' : (["tip"], [MachProbeDict[str(tip)]['z'] for tip in tips]), 
                               },
                    coords={"time": ("time", ref_time), "shot": ("shot", shots), "tip":("tip",tips)},
                    )
    return ds

def build_Bdot_dataset(shots, shot_null, coils, BProbeDict):
    
    ref_data, ref_time = shot_data_Bdot(1231024012, '3B2', BProbeDict)  
    
    ds = xr.Dataset(
                    data_vars={
                               coil: (
                                     ["shot", "time"],
                                     np.array([shot_data_Bdot(shot_null + shot, coil, BProbeDict, ref_time=ref_time
                                                             )[0] for shot in shots]),
                                    )
                                for coil in coils
                               },
                               coords={"time": ("time", ref_time), "shot": ("shot", shots)},
                    )
    return ds

def build_rj_dataset(shots, shot_null, tips, shot_settings, MachProbeDict):
    r"""
    Build xarray of rj values.
    """
    rj_names = {}
    for tip in tips:
        rj_names[str(tip)] = MachProbeDict[str(tip)]['Resistance']
    ds = xr.Dataset(
                    data_vars={
                               'RJ': (
                                    ["tip","shot"],
                                    [np.asarray(shot_settings[rj_names[str(tip)]]) for tip in tips])
                              },
                              coords={"shot": ("shot", shots), "tip": ("tip",tips)},        
    )
    
    return ds

def remove_offset(data, bk_start=-0.015, bk_end=-0.001):
    data = data - data.sel(time=slice(bk_start, bk_end)).mean()
    return data

def box_filter(data, points=50):
    box = np.ones(points)/points
    data_smooth = np.convolve(data, box, mode='same')
    return data_smooth

def convolve(data, width):
    #print(data, width)
    box = np.ones(width)/width
    return np.convolve(data, box, 'same')

def get_data(name='probe3_01', shot=1210719051, tree='mst', comp='dave',
    #tmin=-0.015, tmax=0.125):
    tmin=None, tmax=None):
    server = comp + '.physics.wisc.edu'
    signalname = '\\' + name
    conn = mds.Connection(server)
    conn.openTree(tree, shot)
    node = conn.get(signalname)
    y = node.data()
    x = conn.get('DIM_OF(' + signalname + ')')
    if tmin is not None:
        wh = np.where(x >= tmin)[0]
        y = y[wh]
        x = x[wh]
    if tmax is not None:
        wh = np.where(x < tmax)[0]
        y = y[wh]
        x = x[wh]
    return x, y


def smooth(x, n, axis=None, periodic=False, stype='mean'):
    """Midpoint boxcar smooth of size n for input data x.  If an even n
    is given, this adds 1 to it for the smoothing number -- note this
    is different than IDL behavior (which doubles it and then adds 1).
    """
    n = int(n)
#    if n%2 == 0:
    if n == 1:
        return x
    if n//2 == n/2.0:
        n += 1
    y = np.zeros(shape=np.shape(x))
    if stype == 'mean':
        for i in range(-n//2+1, n//2+1):
            y = y + np.roll(x, i, axis=axis)
    elif stype == 'rms':
        for i in range(-n//2+1, n//2+1):
            y = y + np.roll(x, i, axis=axis)**2
    if stype == 'mean':
        y = y/n
    elif stype == 'rms':
        y = np.sqrt(y/n)
    if periodic == False:
        if x.ndim > 1:
            x = np.moveaxis(x, axis, 0)
            y = np.moveaxis(y, axis, 0)
        if stype == 'mean':
            for j in range(n//2):
                y[j] = np.mean(x[:2*j+1])
                y[-j-1] = np.mean(x[-2*j-1:])
        elif stype == 'rms':
            for j in range(n//2):
                y[j] = rms(x[:2*j+1])
                y[-j-1] = rms(x[-2*j-1:])
        if x.ndim > 1:
            return np.moveaxis(y, 0, axis)
    return y

def takeRatio(ds,dividend,divisor,k=1.1,signal_slice=slice(0e-3, 65e-3),log=True,plot=True,title=''):
    """
    Given a Dataset `ds` with arrays given by single-letter variables, take product of letters given in `dividend` and `divisor` strings separately, then take quotient of dividend/divisor. Optionally take natural `log` and multiply final array by -1/k. 
    """
    log_title=''
    
    top=ds[dividend[0]]
    
    if len(dividend)>1:
        for i in dividend[1:]:
            top=top*ds[i]
    
    bottom=ds[divisor[0]]
    
    if len(divisor)>1:
        for i in divisor[1:]:
            bottom=bottom*ds[i]
            
    ratio=top/bottom
    
    if log:
        ratio=-(1/k)*np.log(ratio)
        log_title='Natural Log of '
    
    if plot:
        fig,ax=plt.subplots(figsize=(20,15))
        
        ratio.sel(time=signal_slice).plot(ax=ax, cmap='inferno',robust=True)
        
        if title == '':
            title='{}{}/{}'.format(log_title,dividend, divisor)
        
        ax.xaxis.set_major_formatter(time_formatter)
        ax.set_title(title)
        
    return ratio           

def extend_subplot(fig,r,c=1,n=None,sharex=True,sharey=False):
    if n is None:
        n=r*c
    ax=fig.add_subplot(r,c,n)
    gs=gridspec.GridSpec(r,c)
    for i in range(r):
        for j in range(c):
            x=c*i+j         #Consecutive index of axes in flat list
            y=x//r+c*(x%r)  #Corresponding location in cXr GridSpec grid
            try:
                fig.axes[x].set_position(gs[y].get_position(fig))
                fig.axes[x].set_subplotspec(gs[y])
            except:
                continue
    return ax

def Probe_summary_plot(ds, refs=[10], signal_time = slice(0e-3, 65e-3), median=None, 
                       shot=None, title='', fig=None, figsize=(20,40), ylim=None,
                       sharey='row', groupy=False, sawteeth=None):
    
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    varDict={}
    
    if median:
        shotslice=slice(median[0],median[-1])
    else:
        shotslice=slice(ds.shot[0],ds.shot[-1])            
    
    for v in list(ds.keys()):
        if 'tip' in ds[v].coords:
            for tip in ds[v].coords['tip'].data:
                    varDict[v + str(tip)] = ds[v].sel(time=signal_time,shot=shotslice,tip=tip)
        else:
            varDict[v] = ds[v].sel(time=signal_time,shot=shotslice)
    
    if fig is None:
        fig = plt.figure(figsize=figsize)
    
    n=-1
    
    if shot:   #If singular shot is specified, generate linear profile plot
        
        fig.set_constrained_layout(False)
        
        ax=fig.subplots(len(varDict), sharex=True, sharey=sharey)
        
        for v, vv in varDict.items():
            n+=1
            vv.sel(shot=shot).plot(ax=ax[n])
            ax[n].set_title('')
            ax[n].set_xlabel('')
            ax[n].set_ylabel(v)
            
            if groupy and not sharey:   #Share y-limit between profiles of like-variables
                if n>0 and v[:-1]==list(varDict.keys())[n-1][:-1]:
                    yrange=ax[n].get_ylim()            
                    if np.min(ylim)>np.min(yrange):
                        ylim[0]=np.min(yrange)
                    if np.max(ylim)<np.max(yrange):
                        ylim[1]=np.max(yrange)
                    ax[n].set_ylim(ylim[0],ylim[1])
                    ax[n-1].set_ylim(ylim[0],ylim[1])
                else:
                    ylim=[*ax[n].get_ylim()]
        
        fig.axes[-1].set_xlabel('time')
        fig.axes[-1].xaxis.set_major_formatter(time_formatter)
        plt.subplots_adjust(wspace=0, hspace=0.01)
        fig.axes[0].set_title(title)
        
        if sawteeth is not None:
            for ax in fig.axes:
                [ax.axvline(s, color='black',linestyle='dotted') for s in sawteeth if s > signal_time.start and s < signal_time.stop]
            
    else:   #If a singular 'shot' is not given, generate shot summary grid plot
        
        fig.set_constrained_layout(True)
        
        ax=fig.subplots(2, len(varDict), sharex=True, sharey=sharey)
        
        if ylim is None:
            ylim=[0,0]
        
        for v, vv in varDict.items():
            n+=1
            vv.plot(ax=ax[0,n], robust=True, cmap='inferno')
            ax[0,n].set_title(v, pad=10)
            ax[0,n].xaxis.set_major_formatter(time_formatter)
            ax[0,n].set_xlabel('')
            ax[0,n].set_ylabel('')
            ax[0,n].tick_params(axis='both', left=True, bottom=True,
                               labelleft=True, labelbottom=True)
            
            for c, r in enumerate(refs):
                ax[0,n].axhline(r, color=colors[c])
                vv.sel(shot=r).plot(ax=ax[1,n])
                
            if median:
                vv.sel(shot=shotslice).median(dim='shot').plot(ax=ax[1,n],color='k',label='Median')
            
            ax[1,n].set_title('')
            ax[1,n].xaxis.set_major_formatter(time_formatter)
            ax[1,n].tick_params(axis='both', left=True, bottom=True,
                               labelleft=True, labelbottom=True)
            
            
            if groupy and not sharey:   #Share y-limit between profiles of like-variables
                if n>0 and v[:-1]==list(varDict.keys())[n-1][:-1]:
                    yrange=ax[1,n].get_ylim()            
                    if np.min(ylim)>np.min(yrange):
                        ylim[0]=np.min(yrange)
                    if np.max(ylim)<np.max(yrange):
                        ylim[1]=np.max(yrange)
                    ax[1,n].set_ylim(ylim[0],ylim[1])
                    ax[1,n-1].set_ylim(ylim[0],ylim[1])
                else:
                    ylim=[*ax[1,n].get_ylim()]            
        
            if ylim != [0,0] and not groupy:
                ax[1,n].set_ylim(ylim[0],ylim[1])
            
        for axs in fig.axes:
            if axs.get_label()=='<colorbar>':
                axs.set_ylabel('')
        
        ax[0,0].set_ylabel('Shot')
        if median:
            refs.append('Median')
        ax[1,0].legend(refs,loc=3)
        fig.suptitle(title)

    plt.show()
    
    return fig
            
    '''
    EXPERIMENTAL PLOTTING SCHEME USING extend_subplot() - Very buggy

    for v in varList:
        if 'tip' in ds[v].coords:
            for tip in ds[v].coords['tip'].values:
                n+=1
                ax=extend_subplot(fig,n)
                ds[v].sel(shot=shot,time=signal_time,tip=tip).plot(x='time',ax=ax)
                ax.set_title('')
                ax.set_xlabel('')
                ax.set_ylabel('{} {}'.format(v,tip))

        else:
            n+=1
            ax=extend_subplot(fig,n)
            ds[v].sel(shot=shot,time=signal_time).plot(x='time',ax=ax)
            ax.set_title('')
            ax.set_xlabel('')

    for v in varList:
        if 'tip' in ds[v].coords:
            ylim=[0,0]
            for tip in ds[v].tip.values:
                n+=1
                ax1=extend_subplot(fig,2,c=n,n=n)
                ds[v].sel(tip=tip,time=signal_time).plot(ax=ax1, robust=True,
                                                         cmap='inferno',
                                                         add_colorbar=False)

                ax1.set_title('{} {}'.format(v,tip))
                ax1.xaxis.set_major_formatter(time_formatter)
                ax1.set_xlabel('')
                ax1.set_ylabel('')

                ax2=extend_subplot(fig,2,c=n,n=2*n)

                for c, r in enumerate(refs):
                    ds[v].sel(shot=r, time=signal_time, tip=tip).plot(ax=ax2)
                    ax1.axhline(r, color=colors[c])

                ax2.set_title('')
                ax2.xaxis.set_major_formatter(time_formatter)
                ax2.set_ylabel('{} {}'.format(v,tip))

                #Share y-limit between profiles of like-variables

                yrange=ax2.get_ylim()            
                if np.min(ylim)>np.min(yrange):
                    ylim[0]=np.min(yrange)
                if np.max(ylim)<np.max(yrange):
                    ylim[1]=np.max(yrange)
                ax2.set_ylim(ylim[0],ylim[1])

        else:
            n+=1
            ax1=extend_subplot(fig,2,c=n,n=n)
            ds[v].sel(time=signal_time).plot(ax=ax1, robust=True,
                                             cmap='inferno',
                                             add_colorbar=False)

            ax1.set_title(v)
            ax1.xaxis.set_major_formatter(time_formatter)
            ax1.set_xlabel('')
            ax1.set_ylabel('')

            ax2=extend_subplot(fig,2,c=n,n=2*n)

            for c, r in enumerate(refs):
                ds[v].sel(shot=r, time=signal_time).plot(ax=ax2)
                ax1.axhline(r, color=colors[c])

            ax2.set_title('')
            ax2.xaxis.set_major_formatter(time_formatter)

    for axs in fig.axes:
        if axs.collections:
            fig.colorbar(axs.collections[0],ax=axs)

    if sharey:
        yranges=[[*fig.axes[i].get_ylim()] for i in range(len(fig.axes)) if i%2]
        ylim=(np.min(yranges),np.max(yranges))

        for i in range(len(fig.axes)):
            if i%2:
                fig.axes[i].set_ylim(ylim)
    '''

def deg_to_rad(deg):
    return deg*np.pi/180.0


def rad_to_deg(rad):
    return rad*180.0/np.pi


def probe_angles(vtype='octahedron', delphi=0.0):
    """Basic Mach probe tip phi and theta angles in radians for
    different vtypes, 'octahedron' or 'tetrahedron'.
    Enter azimuthal offset angle delphi in degrees.
    """
    base_theta = np.array([54.73561, 125.26439])
    if vtype.startswith('octa'):
        n_build = 3
    elif vtype.startswith('tetra'): 
        n_build = 2
    theta = np.tile(base_theta, n_build)
    phi = 360.0*np.arange(2*n_build)/(2*n_build)
    phi += delphi
    return deg_to_rad(phi), deg_to_rad(theta)


def trig_functions(phi, theta):
    """The xbar, ybar, and zbar functions as an array given phi, theta.
    """
    xbar = np.sin(theta)*np.cos(phi)
    ybar = np.sin(theta)*np.sin(phi)
    zbar = np.cos(theta)
    return np.array((xbar, ybar, zbar))


def probe_xyz_values(vtype='octahedron', delphi=0.0):
    """Basic Mach probe tip xbar, ybar, zbar values as an array for
    different vtypes, 'octahedron' or 'tetrahedron'.
    Enter azimuthal offset angle delphi in degrees.
    """
    return trig_functions(*probe_angles(vtype, delphi))


def rotate_x(th):
    """Matrix for rotation around the x axis."""
    return np.array((
        (1.0, 0.0, 0.0),
        (0.0, np.cos(th), -np.sin(th)),
        (0.0, np.sin(th), np.cos(th))
        )) 


def rotate_y(th):
    """Matrix for rotation around the y axis."""
    return np.array((
        (np.cos(th), 0.0, np.sin(th)),
        (0.0, 1.0, 0.0),
        (-np.sin(th), 0.0, np.cos(th))
        )) 


def rotate_z(th):
    """Matrix for rotation around the z axis."""
    return np.array((
        (np.cos(th), -np.sin(th), 0.0),
        (np.sin(th), np.cos(th), 0.0), 
        (0.0, 0.0, 1.0)
        ))


def rotate_xyz(r, thx=0.0, thy=0.0, thz=0.0):
    """
    Rotate set of points first around x axis, then y, then z.
    Enter angles in degrees.
    """
    thx = deg_to_rad(thx)
    thy = deg_to_rad(thy)
    thz = deg_to_rad(thz)
    return np.dot(rotate_z(thz), np.dot(rotate_y(thy),
        np.dot(rotate_x(thx), r)))

def find_centroid(points,plot=False):
    """
    Find the centroid or center of mass of four non-coplanar points in x,y,z space.
    Provide points as list of [x,y,z] triplets. Returns Centroid point [x,y,z]
    """
    n=len(points)
    
    x = sum([points[i][0] for i in range(n)])/n
    y = sum([points[i][1] for i in range(n)])/n
    z = sum([points[i][2] for i in range(n)])/n
    
    if plot:
        fig, ax = plt.subplots(subplot_kw=dict(projection='3d'),figsize=(10,10))
        
        for i in range(n):
            ax.plot(points[i][0],points[i][1],points[i][2],marker='.',color='blue')
            ax.text(points[i][0],points[i][1],points[i][2],str(i+1))
            ax.plot([points[i][0],x],[points[i][1],y],[points[i][2],z],color='orange')
            
        ax.plot(x,y,z,marker='.',color='red')
        ax.text(x,y,z,'C')
        
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.view_init(45,65,'z')
    
    return x, y, z
        
def group_shots(df,first_shot,filters=['x_from_wall','orientation'],plot=True):
    
    SS = df[[*filters,'Shot']].sort_values(by=filters)
    
    SR = SS.value_counts(subset=['x_from_wall','orientation'],sort=False).reset_index(name='count')

    SF = np.array([int(SS.Shot.iloc[i-1]) for i in np.cumsum(SR['count'].values)])-int(first_shot)+1

    SI = SF-SR['count'].values+1

    SD = pd.concat([SR,pd.Series(SI,name='initial_shot'),pd.Series(SF,name='final_shot')],axis=1)
    
    CL = list(SD.columns.values)

    CL.remove('count')

    CL.append('count')

    SD = SD[CL]
    
    if plot:
        fig, ax = plt.subplots(figsize=(30,4))
        ax2=ax.twinx()

        df.plot(y=filters[1], ax=ax, color='blue', linewidth=5)
        ax.set_xlabel('Shot Number')
        ax.set_ylabel('Angle (degrees)', color='blue')
        ax.set_yticks([0,90,180,270])
        ax.legend(loc=2)

        [ax.axvline(i-0.5,color='k') for i in SD.final_shot]

        df.plot(y=filters[0], ax=ax2, color='red', linewidth=5)
        ax2.set_ylabel('Distance (cm)', color='red')
        ax2.set_yticks([0,2,4,6,8,10,12])
        ax2.set_ylim(-0.5,12.5)
        ax2.legend(loc=4)

    return SD
    
def calculate_Mach(ds, tipDic, IsatRatios = ['ab/cd','ac/bd','ad/bc'], 
                   roll_window = 50, smooth = 'median', k=1.1):
    """
    Calculate Mach coefficients given a DataSet (ds) and a Tip Dictionary (tipDic).
    """
    #Prepare Isat Ratio arrays
    
    print("Preparing Isat Ratio Vectors...")
    print(f"Smoothing set to {smooth}")
    
    #Smooth Isat arrays using xarray rolling method    
    Lr = ds[['Isat']].rolling(time=roll_window,center=True,min_periods=1)
    
    if smooth == 'median':
        IsatSet = {letter: Lr.median()['Isat'].sel(tip=tip) for letter, tip in tipDic.items()}
    elif smooth == 'mean':
        IsatSet = {letter: Lr.mean()['Isat'].sel(tip=tip) for letter, tip in tipDic.items()}
    elif smooth == None:
        IsatSet = {letter: ds['Isat'].sel(tip=tip) for letter, tip in tipDic.items()}
    else:
        print("Invalid option for smoothing. Try 'median', 'mean', or 'None'")
        return
    
    #First subtract background and cut minima from Isat arrays
    bk_t0 = ds.time.where(ds.time<0.0,drop=True)[0].data
    bk_tf = ds.time.where(ds.time<0.0,drop=True)[-1].data
    
    for letter in tipDic.keys():
        IsatSet[letter] = IsatSet[letter] - IsatSet[letter].sel(
            time=slice(bk_t0, bk_tf)).mean(dim="time")
        
        IsatSet[letter] = np.maximum(IsatSet[letter], 5e-4)
        
    print(f"Tip Dictionary: {tipDic}")
    print(f"Isat Tip Ratios: {IsatRatios}")  
          
    #Calculate Isat Tip Ratios and RatioVector
    RV={R:takeRatio(IsatSet, *re.split('\/',R), k=1.1, log=True, plot=False
                       ) for R in IsatRatios}
    
    RatioVector = {shot:np.stack([RV[R].sel(shot=shot) for R in IsatRatios]
                                ) for shot in ds.shot.data}
            
    #Calculate and store all M matrices, rotated per shot
    
    print("Calculating ratio Matrix M for each shot...")
      
    tipCoords = {key:np.array([ds.x.sel(tip=tip).data,
                         ds.y.sel(tip=tip).data,
                         ds.z.sel(tip=tip).data]
                       ) for key, tip in tipDic.items()}

    C0 = find_centroid([tipCoords[key] for key in tipDic.keys()],plot=False)

    norm = {key:nl.norm(tipCoords[key]-C0) for key in tipDic.keys()}

    xx = {key:(tipCoords[key][0]-C0[0])/norm[key] for key in tipDic.keys()}
    yy = {key:(tipCoords[key][1]-C0[1])/norm[key] for key in tipDic.keys()}
    zz = {key:(tipCoords[key][2]-C0[2])/norm[key] for key in tipDic.keys()}

    M=np.zeros((len(IsatRatios),3))

    for i, R in enumerate(IsatRatios):
        w, v = re.split('\/',R)
        M[i,0] = np.sum([xx[j] for j in w]) - np.sum([xx[k] for k in v])
        M[i,1] = np.sum([yy[j] for j in w]) - np.sum([yy[k] for k in v])
        M[i,2] = np.sum([zz[j] for j in w]) - np.sum([zz[k] for k in v])   

    rotM_inv={}
    centroid={}

    for shot in ds.shot.data:

        delphi = np.radians(ds.orientation.sel(shot=shot))

        rot = np.array([[ np.cos(delphi), np.sin(delphi), 0],
                        [-np.sin(delphi), np.cos(delphi), 0],
                        [              0,              0, 1]])

        centroid[shot] = find_centroid([tipCoords[key]@rot for key in tipDic.keys()])

        rotM = M@rot

        rotM_inv[shot] = nl.inv(rotM)
        
    #Calculate Mach profiles and store data in new xarray dataset MM
          
    print("Calculating Mach profiles...")
    
    Mach = np.array([rotM_inv[shot] @ RatioVector[shot] for shot in ds.shot.data])
    
    MM = xr.Dataset(coords={"shot": ("shot", ds.shot.data), 
                            "time": ("time", ds.time.data), 
                            "position": ("position", ['x','y','z'])})
    
    MM['M_theta'] = (('shot','time'), Mach[:, 0, :])
    MM['M_phi'] = (('shot','time'), Mach[:, 1, :])
    MM['M_r'] = (('shot','time'), Mach[:, 2, :])
    MM['centroid']=(('shot','position'), np.array(np.round(list(centroid.values()),5)))
    MM['orientation'] = ds.orientation
    MM['d_from_wall'] = ds.d_from_wall
    
    print("Done!")
    
    return MM
        
        
def bar_differences(vtype='octahedron', rx=0.0, ry=0.0, rz=0.0):
    """Mach probe matrix element differences for different vtypes,
    'octahedron' or 'tetrahedron'.
    Enter rotation angles rx, ry, rz in degrees.
    """
    xyz = rotate_xyz(probe_xyz_values(vtype), rx, ry, rz)
    xbar, ybar, zbar = xyz[0], xyz[1], xyz[2]
    x, y, z = {}, {}, {}
    if vtype.startswith('octa'):
        x['ad'] = xbar[0] - xbar[3]
        x['be'] = xbar[1] - xbar[4]
        x['cf'] = xbar[2] - xbar[5]
        y['ad'] = ybar[0] - ybar[3]
        y['be'] = ybar[1] - ybar[4]
        y['cf'] = ybar[2] - ybar[5]
        z['ad'] = zbar[0] - zbar[3]
        z['be'] = zbar[1] - zbar[4]
        z['cf'] = zbar[2] - zbar[5]
    elif vtype.startswith('tetra'): 
        x['abcd'] = xbar[0] + xbar[1] - xbar[2] - xbar[3]
        x['bcda'] = xbar[1] + xbar[2] - xbar[3] - xbar[0]
        x['acbd'] = xbar[0] + xbar[2] - xbar[1] - xbar[3]
        y['abcd'] = ybar[0] + ybar[1] - ybar[2] - ybar[3]
        y['bcda'] = ybar[1] + ybar[2] - ybar[3] - ybar[0]
        y['acbd'] = ybar[0] + ybar[2] - ybar[1] - ybar[3]
        z['abcd'] = zbar[0] + zbar[1] - zbar[2] - zbar[3]
        z['bcda'] = zbar[1] + zbar[2] - zbar[3] - zbar[0]
        z['acbd'] = zbar[0] + zbar[2] - zbar[1] - zbar[3]
    return x, y, z
    

def angle_matrix(vtype='octahedron', rx=0.0, ry=0.0, rz=0.0):
    """Mach probe angle matrix for different vtypes, 'octahedron' or
    'tetrahedron'.
    Enter rotation angles rx, ry, rz in degrees.
    """
    x, y, z = bar_differences(vtype, rx, ry, rz)
    if vtype.startswith('octa'):
        m = np.array((
            (x['ad'], y['ad'], z['ad']),
            (x['be'], y['be'], z['be']),
            (x['cf'], y['cf'], z['cf'])))
    elif vtype.startswith('tetra'): 
        m = np.array((
            (x['abcd'], y['abcd'], z['abcd']),
            (x['bcda'], y['bcda'], z['bcda']),
            (x['acbd'], y['acbd'], z['acbd'])))
    return m


def merge_shots(time, time2, isat, isat2,
                missing_tip, missing_tip2, merge_method):
    r"""
    Merge data from two shots with the probe rotated 180 degrees.
    """
    time_merged = {}
    isat_merged = {}
    time_arrays_equal = True
    for key in list(isat.keys())[1:]:
        if np.array_equal(time[list(isat.keys())[0]], time[key]):
            continue
        time_arrays_equal = False
    if not (time_arrays_equal):
        print('Time arrays not all equal')
        return None
    for key in isat.keys():
        time_merged[key] = time[key]
        if ((merge_method == 'first') or (merge_method == 'average')) and (key == missing_tip):
            isat_merged[key] = isat2[key]
        elif ((merge_method == 'second') or (merge_method == 'average')) and (key == missing_tip2):
            isat_merged[key] = isat[key]
        else:
            if merge_method == 'average':
                isat_merged[key] = (isat[key] + isat2[key])/2
            if merge_method == 'first':
                isat_merged[key] = isat[key]
            if merge_method == 'second':
                isat_merged[key] = isat2[key]
    return time_merged, isat_merged


#shot=1210719051
def mach_results(shot=1210818030, nsmooth=None, res='mach',
    delphi=-60.0, tmin=-0.001, tmax=0.07, comp='dave',
    vtype='octahedron', imin=5E-4, k=1.1, second_shot=None,
    tetra_missing_tip=None, rj_set=[20, 20, 20, 20, 2], rj_set2 = [5, 20, 20, 20, 2], 
    tip_num_name = ['4', '1', '3', '2', '5'], tip_num_name2 = ['3', '2', '4', '1', '5'], 
    mds_start='probe3_', merge_method='first'):
    """Return result depending on res keyword .startswith:
        'isat', tip measurements;
        'rat', ratios of isats depending on vtype;
        'log', logarithms of ratios with factors;
        'mat', angle matrix depending on vtype;
        'inv', inverse of angle matrix;
        'mach', three vector components of M.
    vtype is vertex type, 'octahedron' or 'tetrahedron'.
    Enter azimuthal offset angle delphi in degrees.
    """
    isat = {}
    time = {}
    isat2 = {}
    time2 = {}
    if vtype.startswith('octa'):
        tip_abc_name = ['a', 'b', 'c', 'd', 'e', 'f', 'ret']
        tip_num_name = ['1', '4', '5', '2', '3', '6', '7']
    if vtype.startswith('tetra'):
        tip_abc_name = ['a', 'b', 'c', 'd', 'ret']
        #second_tip_abc_name = ['b', 'a', 'd', 'c', 'ret']
        tip_num_name = tip_num_name
        tip_num_name2 = tip_num_name2
    rj = dict(zip(tip_abc_name,
         rj_set))
    rj2 = dict(zip(tip_abc_name,
          rj_set2))
    tip_mds_name = [mds_start + str(n).zfill(2) for n in \
        tip_num_name]   
    tip_mds_name2 = [mds_start + str(n).zfill(2) for n in \
        tip_num_name2]
    mst_name = ['theta', 'phi', 'r']  # ['x', 'y', 'z'] in Cartesian
    
    print("rj", rj)
    print("tip_mds_name", tip_mds_name)
    
    for i in range(len(tip_abc_name)):
        abc = tip_abc_name[i]
        time[abc], isat[abc] = get_data(tip_mds_name[i], shot,
            comp=comp, tmin=tmin, tmax=tmax)
        #
        # if second_shot assume it is with probe rotated by 180 degrees
        
        #print("rj_abc", rj[abc])
        #print("rj2_abc", rj2[abc])
        #print("abc", abc)
        
        if second_shot:
            time2[abc], isat2[abc] = get_data(tip_mds_name2[i], second_shot,
                                              comp=comp, tmin=tmin, tmax=tmax)
            
            if nsmooth is not None:
                isat2[abc] = smooth(isat2[abc], nsmooth)
            isat2[abc] /= rj2[abc]
            isat2[abc] = np.maximum(isat2[abc], imin)

        if nsmooth is not None:
            isat[abc] = smooth(isat[abc], nsmooth)
        isat[abc] /= rj[abc]
        isat[abc] = np.maximum(isat[abc], imin)

    #print("isat", isat)
    #print("isat2", isat2)
    #print("tetra_missing_tip", tetra_missing_tip)
    #print("tip_num_name", tip_num_name.index(str(tetra_missing_tip)))
    #print("tip_num_name2", tip_num_name2.index(str(tetra_missing_tip)))
    #print("tip_abc_name", tip_abc_name[tip_num_name.index(str(tetra_missing_tip))])
    #print("tip_abc_name2", tip_abc_name[tip_num_name2.index(str(tetra_missing_tip))])
    
    if second_shot:
        time, isat = merge_shots(time, time2, isat, isat2,
                                 tip_abc_name[tip_num_name.index(str(tetra_missing_tip))],
                                 tip_abc_name[tip_num_name2.index(str(tetra_missing_tip))],
                                 merge_method=merge_method)

        if merge_method == 'second':
            tip_num_name = tip_num_name2

    for i in range(len(tip_abc_name)):
        abc = tip_abc_name[i]
        if abc == 'a':
            isat['sum'] = np.zeros(np.shape(isat['a']))
        if abc != 'ret':
            isat['sum'] += isat[abc]
    
    time_arrays_equal = True
    for name in tip_abc_name[1:]:
        if np.array_equal(time['a'], time[name]):
            continue
        time_arrays_equal = False
    if (time_arrays_equal):
        time = time['a']
    else:
        print('Time arrays not all equal')
        return None
    if res.lower().startswith('isat'):
        isat['t'] = time
        isat['name'] = dict(zip(tip_num_name, tip_abc_name))
        isat['rj'] = rj
        isat['units'] = 'A'
        return isat
    if vtype.startswith('octa'):
        ratio = np.array((isat['a']/isat['d'], isat['b']/isat['e'],
            isat['c']/isat['f']))
        rat_name = ['a/d', 'b/e', 'c/f']
    elif vtype.startswith('tetra'):
        ratio = np.array((isat['a']*isat['b']/isat['c']/isat['d'],
            isat['b']*isat['c']/isat['d']/isat['a'],
            isat['c']*isat['a']/isat['b']/isat['d']))
        rat_name = ['a*b/c/d', 'b*c/d/a', 'c*a/b/d']
    if res.startswith('rat'):
        return {'rat':ratio, 't':time, 'name':rat_name, 'units':None}
    log = - 1.0/k*np.log(ratio)
    if res.startswith('log'):
        return {'log':log, 't':time, 'name':rat_name, 'units':None}
    mat = angle_matrix(vtype=vtype, rz=delphi)
    if res.startswith('mat'):
        return {'mat':mat, 'mst_name':mst_name, 'ratio_name':rat_name}
    inv = nl.inv(mat)
    if res.startswith('inv'):
        return {'inv':inv, 'mst_name':mst_name, 'ratio_name':rat_name}
    vel = np.dot(inv, log)
    if res.startswith('mach'):
        return {'t':time, 'r':vel[mst_name.index('r')],
            'theta':vel[mst_name.index('theta')],
            'phi':vel[mst_name.index('phi')], 'units':'M'}


def plot_mach_results(shot=1220927065, second_shot=1220927077, nsmooth=51,
                      res='mach', delphi=-60.0, tmin=-0.001, tmax=0.07, comp='dave',
                      vtype='tetra', imin=5E-4, k=1.1, plot_sum=True,
                      tetra_missing_tip=4, mds_start='probe5_',
                      rj_set=[20, 20, 20, 20, 5], rj_set2=[20, 20, 20, 5, 5],
                      tip_num_name = ['4', '1', '3', '2', '5'], tip_num_name2 = ['3', '2', '4', '1', '5'],
                      merge_method='first', ylim=None):
    """Plot result depending on res keyword .startswith:
        'isat', tip measurements;
        'rat', ratios of isats depending on vtype;
        'log', logarithms of ratios with factors;
        'mach', three vector components of M.
    For other res keywords, just print data.
    vtype is vertex type, 'octahedron' or 'tetrahedron'.
    Enter azimuthal offset angle delphi in degrees.
    """
    #import mpl_toolkits.axes_grid.anchored_artists as maa
    # This looks easier:
    import matplotlib.offsetbox as mo
    data = mach_results(shot,
                        nsmooth, res, delphi, tmin, tmax, comp,
                        vtype, imin, k,
                        second_shot=second_shot,
                        tetra_missing_tip=tetra_missing_tip,
                        rj_set=rj_set,
                        rj_set2=rj_set2,
                        mds_start=mds_start, merge_method=merge_method)
    do_plot = True
    if res.lower().startswith('isat'):
        plot_name = list(data['name'].keys())
        plot_name.sort()
        plot_ylabel = 'I [' + data['units'] + ']'
        plot_data = {k: data[data['name'][k]]
            for k in data['name'].keys()}
    elif res.startswith('rat') or res.startswith('log'):
        plot_name = data['name']
        plot_ylabel = None
        plot_data = {k: data[res[:3]][data['name'].index(k)]
            for k in data['name']}
    elif res.startswith('mach'):
        plot_name = ['r', 'theta', 'phi']
        plot_ylabel = 'v [' + data['units'] + ']'
        plot_data = data
    else:
        do_plot = False
        for k in data.keys():
            print(k, data[k])
    if do_plot:
        plt.clf()
        plt.suptitle(', '.join([
            ' '.join([vtype.capitalize(), 'Mach probe']), str(shot)]))
        for i, p in enumerate(plot_name):
            plt.subplot(len(plot_name), 1, i+1)
            plt.plot(data['t'], plot_data[p], label=p)
                # Label only used if legend is called, see below.)
            plt.grid(True)
            if plot_ylabel is not None:
                plt.ylabel(plot_ylabel)
            if i < len(plot_name) - 1:
                plt.gca().axes.add_artist(mo.AnchoredText(p,
                    loc='upper right'))
                plt.gca().axes.xaxis.set_ticklabels([])
            else:
                if res.startswith('isat'):
                    if plot_sum:
                        #mp.plot(data['t'], data['sum'], alpha=0.5)
                        plt.plot(data['t'], data['sum'], label='sum')
                        plt.legend(loc='right')
                    else:
                        plt.gca().axes.add_artist(mo.AnchoredText(p,
                            loc='upper right'))
                else:
                    plt.gca().axes.add_artist(mo.AnchoredText(p,
                        loc='upper right'))
                plt.xlabel('t [s]')
            if ylim:
                plt.ylim(ylim)
    plt.show()    

