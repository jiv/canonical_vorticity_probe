from sys import version_info
if version_info.major >= 3:
    from importlib import reload
import numpy as np
import numpy.linalg as nl
import numpy.random as nr
import matplotlib.pyplot as mp
import scipy as sp


def deg_to_rad(deg):
    return deg*np.pi/180.0


def rad_to_deg(rad):
    return rad*180.0/np.pi


def rms(x, axis=None):
    return np.sqrt(np.mean(x**2, axis=axis))


def probe_angles(vtype='octahedron', delphi=0.0, aligned=False,
    sigma=0.0, nsigma=None, seed=None, pri=False, test=False,
    testing=False):
    """Basic Mach probe tip phi and theta angles in radians for
    different vtypes, 'octahedron' or 'tetrahedron'.
    Enter azimuthal offset angle delphi in degrees.
    Make aligned True for probe angles along Cartesian axes.
    Enter angle uncertainty sigma > 0.0 in degrees to add normally
    distributed uncertainties to all probe angles.
    Enter seed as an integer or as an instance of
    numpy.random.default_rng in order to maintain access to the instance
    outside this function.
    """
    if aligned:
        phi = np.array([0.0, 0.0, 90.0, 180.0, 0.0, 270.0])
        theta = np.array([90.0, 180.0, 90.0, 90.0, 0.0, 90.0])
    else:
        base_theta = np.array([54.73561, 125.26439])
        #label = 'abcdef'
        if vtype == 'octahedron':
            n_build = 3
        elif vtype == 'tetrahedron': 
            n_build = 2
        #label = label[:2*n_build]
        theta = np.tile(base_theta, n_build)
        phi = 360.0*np.arange(2*n_build)/(2*n_build)
        phi += delphi
        if pri:
            print('theta', theta)
            print('phi', phi)
    return_rng = False
    if sigma > 0.0:
        if test:
            phi0 = phi.copy()
            theta0 = theta.copy()
        if testing:
            if type(seed) is nr._generator.Generator:
                return_rng = True
                rng = seed
            else:
                rng = nr.default_rng(seed)
        if nsigma is None:
            phi += sigma*rng.standard_normal(len(phi))
            theta += sigma*rng.standard_normal(len(theta))
        else:
            if type(nsigma) is int:
                nsigma = [nsigma]
            for n in nsigma:
                phi[n] += sigma*rng.standard_normal(1)
            for n in nsigma:
                # Extra for loop ensures same result as nsigma=None for
                # same number of elements.
                theta[n] += sigma*rng.standard_normal(1)
        if test:
            print('phi0', phi0)
            print('phi', phi)
            print('RMS', rms(phi - phi0))
            print('Mean', np.mean(np.abs(phi - phi0)))
            print('theta0', theta0)
            print('theta', theta)
            print('RMS', rms(theta - theta0))
            print('Mean', np.mean(np.abs(theta - theta0)))
    phi = deg_to_rad(phi)
    theta = deg_to_rad(theta)
    if return_rng:
        return phi, theta, rng
    else:
        return phi, theta


def trig_functions(phi, theta):
    """The xbar, ybar, and zbar functions as an array given phi, theta.
    """
    xbar = np.sin(theta)*np.cos(phi)
    ybar = np.sin(theta)*np.sin(phi)
    zbar = np.cos(theta)
    return np.array((xbar, ybar, zbar))


def probe_xyz_values(vtype='octahedron', delphi=0.0, aligned=False,
    sigma=0.0, nsigma=None, seed=None, testing=False):
    """Basic Mach probe tip xbar, ybar, zbar values as an array for
    different vtypes, 'octahedron' or 'tetrahedron'.
    Enter azimuthal offset angle delphi in degrees.
    """
    if testing:
        if type(seed) is nr._generator.Generator:
            phi, theta, seed = probe_angles(vtype, delphi, aligned, sigma,
                nsigma, seed, testing=True)
            return trig_functions(phi, theta), seed
        else:
            return trig_functions(*probe_angles(vtype, delphi, aligned,
                sigma, nsigma, seed, testing=True))
    else:
        return trig_functions(*probe_angles(vtype, delphi, aligned))


def rotate_x(th):
    """Matrix for rotation around the x axis."""
    return np.array((
        (1.0, 0.0, 0.0),
        (0.0, np.cos(th), -np.sin(th)),
        (0.0, np.sin(th), np.cos(th))
        )) 


def rotate_y(th):
    """Matrix for rotation around the y axis."""
    return np.array((
        (np.cos(th), 0.0, np.sin(th)),
        (0.0, 1.0, 0.0),
        (-np.sin(th), 0.0, np.cos(th))
        )) 


def rotate_z(th):
    """Matrix for rotation around the z axis."""
    return np.array((
        (np.cos(th), -np.sin(th), 0.0),
        (np.sin(th), np.cos(th), 0.0), 
        (0.0, 0.0, 1.0)
        ))


def rotate_xyz(r, thx=0.0, thy=0.0, thz=0.0):
    """
    Rotate set of points first around x axis, then y, then z.
    Enter angles in degrees.
    """
    thx = deg_to_rad(thx)
    thy = deg_to_rad(thy)
    thz = deg_to_rad(thz)
    return np.dot(rotate_z(thz), np.dot(rotate_y(thy),
        np.dot(rotate_x(thx), r)))


def bar_differences(vtype='octahedron', rx=0.0, ry=0.0, rz=0.0,
    aligned=False, sigma=0.0, nsigma=None, seed=None, testing=False):
    """Mach probe matrix element differences for different vtypes,
    'octahedron' or 'tetrahedron'.
    Enter rotation angles rx, ry, rz in degrees.
    """
    return_rng = False
    if testing:
        if type(seed) is nr._generator.Generator:
            return_rng = True
            pxyzv, seed = probe_xyz_values(vtype, aligned=aligned,
                sigma=sigma, nsigma=nsigma, seed=seed, testing=True)
            xyz = rotate_xyz(pxyzv, rx, ry, rz)
        else:
            xyz = rotate_xyz(probe_xyz_values(vtype, aligned=aligned,
                sigma=sigma, nsigma=nsigma, seed=seed, testing=True),
                rx, ry, rz)
    else:
        xyz = rotate_xyz(probe_xyz_values(vtype, aligned=aligned), rx,
            ry, rz)
    xbar, ybar, zbar = xyz[0], xyz[1], xyz[2]
    x, y, z = {}, {}, {}
    if vtype == 'octahedron':
        x['ad'] = xbar[0] - xbar[3]
        x['be'] = xbar[1] - xbar[4]
        x['cf'] = xbar[2] - xbar[5]
        y['ad'] = ybar[0] - ybar[3]
        y['be'] = ybar[1] - ybar[4]
        y['cf'] = ybar[2] - ybar[5]
        z['ad'] = zbar[0] - zbar[3]
        z['be'] = zbar[1] - zbar[4]
        z['cf'] = zbar[2] - zbar[5]
    elif vtype == 'tetrahedron': 
        x['abcd'] = xbar[0] + xbar[1] - xbar[2] - xbar[3]
        x['bcda'] = xbar[1] + xbar[2] - xbar[3] - xbar[0]
        x['acbd'] = xbar[0] + xbar[2] - xbar[1] - xbar[3]
        y['abcd'] = ybar[0] + ybar[1] - ybar[2] - ybar[3]
        y['bcda'] = ybar[1] + ybar[2] - ybar[3] - ybar[0]
        y['acbd'] = ybar[0] + ybar[2] - ybar[1] - ybar[3]
        z['abcd'] = zbar[0] + zbar[1] - zbar[2] - zbar[3]
        z['bcda'] = zbar[1] + zbar[2] - zbar[3] - zbar[0]
        z['acbd'] = zbar[0] + zbar[2] - zbar[1] - zbar[3]
    if return_rng:
        return x, y, z, seed
    else:
        return x, y, z
    

def angle_matrix(vtype='octahedron', rx=0.0, ry=0.0, rz=0.0,
    aligned=False, sigma=0.0, nsigma=None, seed=None, testing=False):
    """Mach probe angle matrix for different vtypes, 'octahedron' or
    'tetrahedron'.
    Enter rotation angles rx, ry, rz in degrees.
    """
    return_rng = False
    if testing:
        if type(seed) is nr._generator.Generator:
            return_rng = True
            x, y, z, seed = bar_differences(vtype, rx, ry, rz, aligned,
                sigma, nsigma, seed, testing=True)
        else:
            x, y, z = bar_differences(vtype, rx, ry, rz, aligned, sigma,
                nsigma, seed, testing=True)
    else:
            x, y, z = bar_differences(vtype, rx, ry, rz, aligned)
    if vtype == 'octahedron':
        m = np.array((
            (x['ad'], y['ad'], z['ad']),
            (x['be'], y['be'], z['be']),
            (x['cf'], y['cf'], z['cf'])))
    elif vtype == 'tetrahedron': 
        m = np.array((
            (x['abcd'], y['abcd'], z['abcd']),
            (x['bcda'], y['bcda'], z['bcda']),
            (x['acbd'], y['acbd'], z['acbd'])))
    if return_rng:
        return m, seed
    else:
        return m


def probe_measurement_test(vtype='octahedron', vseed=None, pseed=None,
    sigma=1.0, nsigma=None, aligned=False, ttype='histogram',
    ntrials=100000, nbins=100, alpha=0.02, plo=True, color=None,
    ret=False, clf=True, label=None, fontsize=None):
    """Do ntrials random trials of measuring a random flow and plot
    deviations.
    With ttype == 'rotation', rotate whole probe to a different random
    angle in each trial.
    This is really just a test of Numpy matrix inverse but is the
    best I can think of at the moment.
    With ttype == 'sigma', add random uncertainties to probe angles in
    each trial.
    With ttype == 'histogram', make histogram of sigma data.
    Default fontsize for legend is 10.
    """
    radsincirc = 2.0*np.pi
    degsincirc = 360.0
    rng = nr.default_rng(vseed)
    theta = radsincirc*rng.random()
    phi = radsincirc*rng.random()
    flow = trig_functions(phi, theta) 
    delta = np.zeros(ntrials)
    rng = nr.default_rng(pseed)
    if ttype == 'rotation':
        suptitle = 'Probe rotation measurement test'
        title = 'Random probe rotations'
        xrots = degsincirc*rng.random(ntrials)
        yrots = degsincirc*rng.random(ntrials)
        zrots = degsincirc*rng.random(ntrials)
    elif ttype == 'sigma' or ttype == 'histogram':
        suptitle = 'Probe angle uncertainty test'
        iseed = pseed
        #title = 'Random probe angle uncertainties'
        Ainv = nl.inv(angle_matrix(vtype, aligned=aligned, sigma=0.0,
            seed=None, testing=True))
    for i in range(ntrials):
        if ttype == 'rotation':
            A = angle_matrix(vtype, xrots[i], yrots[i], zrots[i],
                aligned, testing=True)
            Ainv = nl.inv(A)
        elif ttype == 'sigma' or ttype == 'histogram':
            if i == 0:
                A, pseed = angle_matrix(vtype, aligned=aligned,
                    sigma=sigma, nsigma=nsigma, seed=rng, testing=True)
            else:
                A, pseed = angle_matrix(vtype, aligned=aligned,
                    sigma=sigma, nsigma=nsigma, seed=pseed,
                    testing=True)
        Av = np.dot(A, flow)
        vmeas = np.dot(Ainv, Av)
        #delta[i] = np.abs(np.max(flow - vmeas))
        delta[i] = np.sqrt(np.sum((flow - vmeas)**2))
    # Plot.
    if plo:
        if ttype == 'sigma':
            suptitle = ', '.join([
                vtype.capitalize() + ' ' + suptitle.lower(),
                '\nV = ' + np.array_str(flow, precision=5),
                '|V| = ' + str(np.round(np.sqrt(np.sum(flow**2)),1)),
                '\nflow seed = ' + str(vseed),
                'sigma = ' + str(sigma) + ' deg.',
                'probe seed = ' + str(iseed)])
            title = ', '.join(['Flow measurement deviations',
                #'RMS = ' + '{:7.5f}'.format(rms(delta))]))
                '\nmean = ' + '{:.5g}'.format(np.mean(delta)),
                'RMS = ' + '{:.5g}'.format(rms(delta)),
                'max = ' + '{:.5g}'.format(np.max(delta))])
        elif ttype == 'histogram':
            if label is None:
                suptitle = ', '.join([
                    vtype.capitalize() + ' ' + suptitle.lower(),
                    'trials = ' + str(ntrials), 
                    '\nV = ' + np.array_str(flow, precision=5),
                    '|V| = ' +str(np.round(np.sqrt(np.sum(flow**2)),1)),
                    '\nflow seed = ' + str(vseed),
                    'sigma = ' + str(sigma) + ' deg.',
                    'probe seed = ' + str(iseed)])
                title = ', '.join(['Flow measurement deviations',
                    #'RMS = ' + '{:7.5f}'.format(rms(delta))]))
                    '\nmean = ' + '{:.5g}'.format(np.mean(delta)),
                    'RMS = ' + '{:.5g}'.format(rms(delta)),
                    'max = ' + '{:.5g}'.format(np.max(delta))])
            else:
                suptitle = ', '.join([
                    suptitle, 'trials = ' + str(ntrials), 
                    '\nV = ' + np.array_str(flow, precision=5),
                    '|V| = ' +str(np.round(np.sqrt(np.sum(flow**2)),1)),
                    '\nflow seed = ' + str(vseed),
                    'sigma = ' + str(sigma) + ' deg.',
                    'probe seed = ' + str(iseed)])
                title = 'Flow measurement deviations'
        if clf:
            mp.clf()
        if ttype == 'rotation' or ttype == 'sigma':
            trials = np.arange(1, ntrials+1)
            if ttype == 'rotation':
                mp.suptitle(', '.join([
                    vtype.capitalize() + ' ' + suptitle.lower(),
                    'seed = ' + str(vseed),
                    '\nvelocity = ' + str(flow)]))
                mp.subplot(2, 1, 1)
                mp.title(', '.join([title, 'seed = ' + str(pseed)]))
                mp.plot(trials, xrots, 'o', label='x axis')
                mp.plot(trials, yrots, 'o', label='y axis')
                mp.plot(trials, zrots, 'o', label='z axis')
                mp.legend(loc='upper right')
                mp.gca().axes.xaxis.set_ticklabels([])
                mp.ylabel('degrees')
                mp.subplot(2, 1, 2)
                mp.plot(trials, delta, 'o', color='k')
                mp.subplots_adjust(top=0.8)
            elif ttype == 'sigma':
                mp.suptitle(suptitle)
                mp.subplots_adjust(top=0.75)
                mp.plot(trials, delta, '.', alpha=alpha)
            mp.grid(True)
            mp.title(title)
            mp.xlabel('Trials')
            mp.ylabel('|V_measured - V|/|V|')
        elif ttype == 'histogram':
            hist, x = np.histogram(delta, nbins)
            x = 0.5*(x[:-1] + x[1:])
            mp.suptitle(suptitle)
            if label is None:
                mp.subplots_adjust(top=0.75)
                #mp.plot(x, hist, color=color)
                mp.plot(x, hist, '.', color=color)
            else:
                mp.subplots_adjust(top=0.80)
                #mp.plot(x, hist, label=label, color=color)
                mp.plot(x, hist, '.', label=label, color=color)
                mp.legend(fontsize=fontsize)
            mp.grid(True)
            mp.title(title)
            mp.xlabel('|V_measured - V|/|V|')
            mp.ylabel('Counts')
    # Return.
    if ret:
        return {'phi':phi, 'theta':theta, 'flow':flow, 'delta':delta}


def probe_angle_uncertainty_tests(sav=False, plo=False, pri=True,
    ntrials=10000, ret=False):
    """Do multiple run-throughs of probe_measurement_test for different
    probe angle uncertainties, vtypes, and seeds.
    If plo is true, plots trials series.
    If pri is true, prints trial results.
    If ret is true, returns trial results.
    """
    vtypes = 'octahedron', 'tetrahedron'
    sigmas = 0.3, 1.0, 3.0
    vseeds = np.arange(1, 11)
    pseeds = [0]
    if ret:
        delta = np.zeros((len(vtypes), len(sigmas), len(vseeds),
            len(pseeds), ntrials))
    for vt, vtype in enumerate(vtypes):
        for si, sigma in enumerate(sigmas):
            for vs, vseed in enumerate(vseeds):
                for ps, pseed in enumerate(pseeds):
                    print(vtype, sigma, vseed, pseed)
                    if pri or ret:
                        if plo:
                            data = probe_measurement_test(vtype, vseed,
                                pseed, sigma, ttype='sigma',
                                ntrials=ntrials, plo=True, ret=True)
                        else:
                            data = probe_measurement_test(vtype, vseed,
                                pseed, sigma, ttype='sigma',
                                ntrials=ntrials, plo=False, ret=True)
                    else:
                        if plo:
                            probe_measurement_test(vtype, vseed, pseed,
                                sigma, ttype='sigma', ntrials=ntrials,
                                plo=True, ret=False)
                        else:
                            print('Bad input')
#                            probe_measurement_test(vtype, vseed, pseed,
#                                sigma, ttype='sigma', ntrials=ntrials,
#                                plo=False, ret=False)
                    if pri:
                        print('Median(delta)', np.median(data['delta']))
                        print('Mean(delta)', np.mean(data['delta']))
                        print('RMS(delta)', rms(data['delta']))
                    if ret:
                        delta[vt, si, vs, ps] = data['delta']
                    if plo and sav:
                        for suffix in ('.pdf', '.png'):
                            mp.savefig('_'.join([
                                'probe_ang_unc_meas_hist',
                                'vtype=' + vtype,
                                'sigma='
                                + str(sigma).replace('.', 'pseed'),
                                'vseed=' + str(vseed),
                                'pseed=' + str(pseed)]) + suffix,
                                bbox_inches='tight')
    if ret:
        return {'vtype':vtypes, 'sigma':sigmas, 'vseed':vseeds,
            'pseed':pseeds, 'delta':delta.squeeze()}


def seed_probe_angle_uncertainty_tests(vseed=6, pseed=1, sav=False):
    """Given seeds, do two run-throughs of probe_measurement_test for
    different probe angle uncertainties and vtypes."""
    sigmas = 0.3, 1.0, 3.0
    for sigma in sigmas:
        print(sigma)
        probe_measurement_test('octahedron', vseed, pseed, sigma,
            clf=True, label='octahedron')
        probe_measurement_test('tetrahedron', vseed, pseed, sigma,
            clf=False, label='tetrahedron')
        if sav:
            mp.savefig('_'.join(['probe_ang_unc_meas_hists',
                'sigma=' + str(sigma).replace('.', 'p'),
                'vseed=' + str(vseed), 'pseed=' + str(pseed)]) + '.pdf',
                bbox_inches='tight')


def partial_probe_angle_uncertainty_histograms(vtype='tetrahedron',
    sigma=1.0, vseed=0, pseed=1, fontsize=None, sav=False, tag=None,
    cmap='turbo'):
    """Do two run-throughs of probe_measurement_test with only some of
    the probe angles uncertain.
    """
    try:
        colors = mp.cm.get_cmap(cmap)(np.linspace(0.0, 1.0, 15))
        #print(cmap)
    except:
        #print(cmap + ' failed, jet')
        colors = mp.cm.get_cmap('jet')(np.linspace(0.0, 1.0, 15))
#    probe_measurement_test(vtype, vseed, pseed, sigma, None,
#        label='None', clf=True, fontsize=fontsize, color=colors[0])
    probe_measurement_test(vtype, vseed, pseed, sigma, range(4),
#        label='range(4)', clf=False, fontsize=fontsize, color=colors[0])
        label='[a, b, c, d]', clf=True, fontsize=fontsize, color=colors[0])
    probe_measurement_test(vtype, vseed, pseed, sigma, [0, 1, 2],
        label='[a, b, c]', clf=False, fontsize=fontsize, color=colors[1])
    probe_measurement_test(vtype, vseed, pseed, sigma, [0, 1, 3],
        label='[a, b, d]', clf=False, fontsize=fontsize, color=colors[2])
    probe_measurement_test(vtype, vseed, pseed, sigma, [0, 2, 3],
        label='[a, c, d]', clf=False, fontsize=fontsize, color=colors[3])
    probe_measurement_test(vtype, vseed, pseed, sigma, [1, 2, 3],
        label='[b, c, d]', clf=False, fontsize=fontsize, color=colors[4])
    probe_measurement_test(vtype, vseed, pseed, sigma, [0, 1],
        label='[a, b]', clf=False, fontsize=fontsize, color=colors[5])
    probe_measurement_test(vtype, vseed, pseed, sigma, [0, 2],
        label='[a, c]', clf=False, fontsize=fontsize, color=colors[6])
    probe_measurement_test(vtype, vseed, pseed, sigma, [0, 3],
        label='[a, d]', clf=False, fontsize=fontsize, color=colors[7])
    probe_measurement_test(vtype, vseed, pseed, sigma, [1, 2],
        label='[b, c]', clf=False, fontsize=fontsize, color=colors[8])
    probe_measurement_test(vtype, vseed, pseed, sigma, [1, 3],
        label='[b, d]', clf=False, fontsize=fontsize, color=colors[9])
    probe_measurement_test(vtype, vseed, pseed, sigma, [2, 3],
        label='[c, d]', clf=False, fontsize=fontsize, color=colors[10])
    probe_measurement_test(vtype, vseed, pseed, sigma, [0],
        label='[a]', clf=False, fontsize=fontsize, color=colors[11])
    probe_measurement_test(vtype, vseed, pseed, sigma, [1],
        label='[b]', clf=False, fontsize=fontsize, color=colors[12])
    probe_measurement_test(vtype, vseed, pseed, sigma, [2],
        label='[c]', clf=False, fontsize=fontsize, color=colors[13])
    probe_measurement_test(vtype, vseed, pseed, sigma, [3],
        label='[d]', clf=False, fontsize=fontsize, color=colors[14])
    if sav:
        if tag is None:
            mp.savefig('_'.join(['partial_probe_ang_unc_meas_hist',
                vtype, 'sigma=' + str(sigma).replace('.', 'p'),
                'vseed=' + str(vseed), 'pseed=' + str(pseed)]) + '.pdf', 
                bbox_inches='tight')
        else:
            mp.savefig('_'.join(['partial_probe_ang_unc_meas_hist',
                vtype, 'sigma=' + str(sigma).replace('.', 'p'),
                'vseed=' + str(vseed), 'pseed=' + str(pseed), tag])
                + '.pdf', bbox_inches='tight')


def gaussian(x, mu=0.0, sigma=1.0):
    return np.exp(-((x - mu)/sigma)**2/2.0)/np.sqrt(2.0*np.pi)/sigma


def multiple_gaussian_distribution_test(ndim=2, nx=301, pri=True,
    rhomax=5.0, nbins=30):
    if ndim > 4:
        print('Changing ndim to 4')
        ndim = 4
    x = np.linspace(-rhomax, rhomax, nx)
    y = gaussian(x)
    z = y
    i = 1
    if pri:
        print('z')
    while i < ndim:
        if pri:
            print('i', str(i))
        z = z[..., np.newaxis]*y
        i += 1
    if pri:
        print('x')
    x = np.array(np.meshgrid(*[x for i in range(ndim)]))
    rho = np.sqrt(np.sum(x**2, axis=0))
    #return rho, z
    bins0 = np.linspace(0.0, rhomax - rhomax/nbins, nbins)
    bins1 = np.linspace(rhomax/nbins, rhomax, nbins)
    bins = 0.5*(bins0 + bins1)
    where = []
    for i in range(nbins):
        where.append(np.where((rho >= bins0[i]) & (rho < bins1[i])))
    totals = np.zeros(nbins)
    for i in range(nbins):
        totals[i] = np.sum(z[where[i]])
    totals = totals/np.max(totals)
    return bins, totals


def plot_multiple_gaussian_distribution_test(cmap='turbo'):
    try:
        colors = mp.cm.get_cmap(cmap)(np.linspace(0.0, 1.0, 4))
    except:
        colors = mp.cm.get_cmap('jet_r')(np.linspace(0.0, 1.0, 4))
    mp.clf()
    rho, z = multiple_gaussian_distribution_test(1, nx=1000, nbins=100)
    mp.plot(rho, z, label='1D', color=colors[3])
    rho, z = multiple_gaussian_distribution_test(2, nx=1000, nbins=90)
    mp.plot(rho, z, label='2D', color=colors[2])
    rho, z = multiple_gaussian_distribution_test(3, nx=300, nbins=60)
    mp.plot(rho, z, label='3D', color=colors[1])
    rho, z = multiple_gaussian_distribution_test(4, nx=101, nbins=30)
    mp.plot(rho, z, label='4D', color=colors[0])
    mp.legend()
    mp.title('Multidimensional Gaussian distributions, sigma=1.0')
    mp.xlabel('rho')
    mp.ylabel('Normalized frequency')


