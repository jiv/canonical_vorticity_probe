import numpy as np
import matplotlib.pyplot as mp
import scipy as sp


def clf_plot(r):
    mp.clf()
    ax = mp.gca(projection='3d')
    mp.plot(*r)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')


def vertices(vtype='octahedron', aligned=False, test=False):
    """If aligned is True, then octahedron points are along Cartesian
    axes.
    To rotate tetrahedron-dual cube with vertices at
    [x, y, z] = +-[1, 1, 1] to put two cube vertices at
    [x, y, z] = [0, 0, +-sqrt(3)], use xyz rotation (not zyx) of
    thx=np.pi/4, thy=-(np.pi/2-np.arctan(np.sqrt(2.0))), thz=0.0.
    Note that the second rotation is by
    -(pi/2 - atan(sqrt(2))) = -0.6154797 = -35.26439 degrees.
    """
    if vtype == 'octahedron':
        x = np.array([1.0, 0.0, 0.0, -1.0, 0.0, 0.0])
        y = np.array([0.0, 0.0, 1.0, 0.0, 0.0, -1.0])
        z = np.array([0.0, -1.0, 0.0, 0.0, 1.0, 0.0])
    elif vtype == 'cube':
        x = np.array([1.0, -1.0, -1.0, 1.0, 1.0, 1.0, -1.0, -1.0])
        y = np.array([1.0, 1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0])
        z = np.array([1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0])
    elif vtype == 'tetrahedron':
        x = np.array([1.0, 0.0, -1.0, 0.0])
        y = np.array([0.0, 1.0, 0.0, -1.0])
        z = np.array([np.sqrt(2.0)/2.0, -np.sqrt(2.0)/2.0,
            np.sqrt(2.0)/2.0, -np.sqrt(2.0)/2.0])
    r = np.array((x, y, z))
    if (vtype == 'octahedron' or vtype == 'cube') and not aligned:
        r = rotate_xyz(r, thx=np.pi/4,
            thy=-(np.pi/2-np.arctan(np.sqrt(2.0))), thz=0.0, test=False)
    if test:
        clf_plot(r)
    return r


def rx(th):
    """Matrix for rotation around the x axis."""
    return np.array((
        (1.0, 0.0, 0.0),
        (0.0, np.cos(th), -np.sin(th)),
        (0.0, np.sin(th), np.cos(th))
        )) 


def ry(th):
    """Matrix for rotation around the y axis."""
    return np.array((
        (np.cos(th), 0.0, np.sin(th)),
        (0.0, 1.0, 0.0),
        (-np.sin(th), 0.0, np.cos(th))
        )) 


def rz(th):
    """Matrix for rotation around the z axis."""
    return np.array((
        (np.cos(th), -np.sin(th), 0.0),
        (np.sin(th), np.cos(th), 0.0), 
        (0.0, 0.0, 1.0)
        ))


def rotate_xyz(r, thx=0.0, thy=0.0, thz=0.0, test=False):
    """Rotate set of points first around x axis, then y, then z."""
    if test:
        clf_plot(r)
    r = np.dot(rz(thz), np.dot(ry(thy), np.dot(rx(thx), r)))
    if test:
        mp.plot(*r)
    return r


def rotate_zyx(r, thx=0.0, thy=0.0, thz=0.0, test=False):
    """Rotate set of points first around z axis, then y, then x."""
    if test:
        clf_plot(r)
    r = np.dot(rz(thz), np.dot(ry(thy), np.dot(rx(thx), r)))
    if test:
        mp.plot(*r)
    return r


def fill_edges(r=vertices('octahedron'), vtype='octahedron', test=False):
    """Add vertices to an array to get all of the represented shape's
    edges."""
    if vtype == 'octahedron':
        for i in [1, 3, 5, 0, 2, 4, 0]:
            r = np.append(r, r[:, i:i+1], axis=1)
    elif vtype == 'cube':
        r = np.append(r, r[:, 2:3], axis=1)
        r2 = rotate_xyz(r, 0, 0, np.pi)
        r = np.append(r, r2, axis=1)
    elif vtype == 'tetrahedron':
        for i in [0, 2, 3, 1]:
            r = np.append(r, r[:, i:i+1], axis=1)
    if test:
        clf_plot(r)
    return r


def radius(r):
    """Find spherical radius for an input Cartesian array."""
    return np.sqrt(np.sum(r**2, axis=0))


def cylindrical_radius(r):
    """Find cylindrical radius for an input Cartesian array."""
    return np.sqrt(r[0, :]**2 + r[1, :]**2)


def theta(r):
    """Find spherical polar angle for an input Cartesian array."""
    return np.arctan2(cylindrical_radius(r), r[2, :])


def phi(r):
    """Find spherical azimuthal angle for an input Cartesian array."""
    return np.arctan2(r[1, :], r[0, :])


def print_vertex_angles(vtype='octahedron', thx=0.0, thy=0.0, thz=0.0,
    aligned=False, nround=1, test=False, fontsize=14, plot_edges=True):
    """Use defaults to show spherical angles of vertices.
    Enter thx, thy, thz in degrees.
    Default fontsize for text function is 10.
    """
    my_vertices = vertices(vtype, aligned=aligned)
    if vtype == 'octahedron':
        my_vertices = rotate_xyz(my_vertices, thx=thx*np.pi/180.0,
            thy=thy*np.pi/180.0, thz=thz*np.pi/180.0)
    elif vtype == 'tetrahedron':
        my_vertices = rotate_xyz(my_vertices, thx=thx*np.pi/180.0,
            thy=thy*np.pi/180.0, thz=thz*np.pi/180.0)
    my_theta = theta(my_vertices)/np.pi*180.0
    my_phi = (phi(my_vertices)/np.pi*180.0)%360.0
    #print('[x', 'y', 'z]', 'theta', 'phi', '[degrees]')
    print('theta', 'phi', '[degrees]')
    for i in range(len(my_theta)):
        ##print(np.round(my_vertices[:, i], nround),
        ##    np.round(my_theta[i], nround), np.round(my_phi[i], nround))
        #print(np.round(my_vertices[:, i], nround),
        #    np.round(my_theta[i], nround), np.round(my_phi[i], nround))
        print(np.round(my_theta[i], nround),
            np.round(my_phi[i], nround))
    if test:
        edge_vertices = fill_edges(my_vertices, vtype)
        mp.clf()
        ax = mp.gca(projection='3d')
        mp.plot(*my_vertices, 'o')
        text_labels = 'abcdefghi'[0:len(my_vertices[0])]
        my_radius = np.max(radius(my_vertices))
        for i, t in enumerate(text_labels):
            ax.text(
                *(my_vertices[:, i] + np.array((0.05*my_radius, 0, 0))),
                t, None, fontsize=fontsize)
        if plot_edges:
            mp.plot(*edge_vertices, linewidth=1)
        len_my_vertices = len(my_vertices[0])
        my_vertices = np.repeat(my_vertices, 2, axis=1)
        for i in range(len_my_vertices):
            my_vertices[:, 2*i] = np.zeros(3)
        mp.plot(*my_vertices)
        ax.plot([0.0], [0.0], [0.0], 'o', color='k')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')


def print_both_cases(test=False, sav=False):
    vtype = ['octahedron', 'tetrahedron']
    plot_edges= [True, False]
    tag = ['edges', 'vertices']
    for v in vtype:
        if not test:
            print(v)
            print_vertex_angles(v, test=test)
        else:
            for i, pe in enumerate(plot_edges):
                print(v)
                print_vertex_angles(v, plot_edges=pe, test=test)
                if sav:
                    filename = '_'.join([v, tag[i] + '.pdf'])
                    mp.savefig(filename, bbox_inches='tight')

