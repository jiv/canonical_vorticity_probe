import numpy as np
import time
import numpy.linalg as nl
from scipy.stats import norm, chi
import matplotlib.pyplot as plt
import matplotlib as mpl
import itertools as itt
import multiprocessing as mpc
from functools import partial

'''
Various methods and functions for use in Full Tetrahedral Mach probe velocity uncertainty analysis

Richard Reksoatmodjo, February 22nd 2024
'''

sin = lambda x : np.sin(np.radians(x))
cos = lambda x : np.cos(np.radians(x))

def calc_AA_matrix(Theta_A = np.degrees(np.arctan(np.sqrt(2))),      #Default upper tetrahedral angle (54.7 degrees)
                   Phi_A = 0,                                        #Phi_A default is 0 degrees  
                   Theta_B = 180-np.degrees(np.arctan(np.sqrt(2))),  #Default lower tetrahedral angle (125.3 degrees)
                   Phi_B = 90,                                       #Phi_B default is 90 degrees
                   Theta_C = np.degrees(np.arctan(np.sqrt(2))),      #Default upper tetrahedral angle (54.7 degrees)
                   Phi_C = 180,                                      #Phi_C default is 180 degrees
                   Theta_D = 180-np.degrees(np.arctan(np.sqrt(2))),  #Default lower tetrahedral angle (125.3 degrees)
                   Phi_D = 270):                                     #Phi_D default is 270 degrees

    A = sin(Theta_A)*cos(Phi_A) - sin(Theta_B)*cos(Phi_B)
    B = sin(Theta_A)*sin(Phi_A) - sin(Theta_B)*sin(Phi_B)
    C = cos(Theta_A) - cos(Theta_B)
    D = sin(Theta_A)*cos(Phi_A) - sin(Theta_C)*cos(Phi_C)
    E = sin(Theta_A)*sin(Phi_A) - sin(Theta_C)*sin(Phi_C)
    F = cos(Theta_A) - cos(Theta_C)
    G = sin(Theta_A)*cos(Phi_A) - sin(Theta_D)*cos(Phi_D)
    H = sin(Theta_A)*sin(Phi_A) - sin(Theta_D)*sin(Phi_D)
    I = cos(Theta_A) - cos(Theta_D)
    
    AA = np.array([[A,B,C],
               [D,E,F],
               [G,H,I]])

    AA = np.round(AA,5)
    
    return AA

sin = lambda x : np.sin(np.radians(x))
cos = lambda x : np.cos(np.radians(x))

def calc_AK_matrix(Theta_A = np.degrees(np.arctan(np.sqrt(2))),      #Default upper tetrahedral angle (54.7 degrees)
                   Phi_A = 0,                                        #Phi_A default is 0 degrees  
                   Theta_B = 180-np.degrees(np.arctan(np.sqrt(2))),  #Default lower tetrahedral angle (125.3 degrees)
                   Phi_B = 90,                                       #Phi_B default is 90 degrees
                   Theta_C = np.degrees(np.arctan(np.sqrt(2))),      #Default upper tetrahedral angle (54.7 degrees)
                   Phi_C = 180,                                      #Phi_C default is 180 degrees
                   Theta_D = 180-np.degrees(np.arctan(np.sqrt(2))),  #Default lower tetrahedral angle (125.3 degrees)
                   Phi_D = 270):                                     #Phi_D default is 270 degrees

    A = sin(Theta_A)*cos(Phi_A) + sin(Theta_B)*cos(Phi_B) - sin(Theta_C)*cos(Phi_C) - sin(Theta_D)*cos(Phi_D)
    B = sin(Theta_A)*sin(Phi_A) + sin(Theta_B)*sin(Phi_B) - sin(Theta_C)*sin(Phi_C) - sin(Theta_D)*sin(Phi_D)
    C = cos(Theta_A) + cos(Theta_B) - cos(Theta_C) - cos(Theta_D)
    D = sin(Theta_B)*cos(Phi_B) + sin(Theta_C)*cos(Phi_C) - sin(Theta_A)*cos(Phi_A) - sin(Theta_D)*cos(Phi_D)
    E = sin(Theta_B)*sin(Phi_B) + sin(Theta_C)*sin(Phi_C) - sin(Theta_A)*sin(Phi_A) - sin(Theta_D)*sin(Phi_D)
    F = cos(Theta_B) + cos(Theta_C) - cos(Theta_A) - cos(Theta_D)
    G = sin(Theta_A)*cos(Phi_A) + sin(Theta_C)*cos(Phi_C) - sin(Theta_B)*cos(Phi_B) - sin(Theta_D)*cos(Phi_D)
    H = sin(Theta_A)*sin(Phi_A) + sin(Theta_C)*sin(Phi_C) - sin(Theta_B)*sin(Phi_B) - sin(Theta_D)*sin(Phi_D)
    I = cos(Theta_A) + cos(Theta_C) - cos(Theta_B) - cos(Theta_D)
    
    AA = np.array([[A,B,C],
               [D,E,F],
               [G,H,I]])

    AA = np.round(AA,5)
    
    return AA

def ranger(Increments = 100,                                 #Half-length of sample range
           Max_Dev_Angle = 5,                                #Maximum deviation
           Theta_A = np.degrees(np.arctan(np.sqrt(2))),      #Default upper tetrahedral angle (54.7 degrees)
           Phi_A = 0,                                        #Phi_A default is 0 degrees  
           Theta_B = 180-np.degrees(np.arctan(np.sqrt(2))),  #Default lower tetrahedral angle (125.3 degrees)
           Phi_B = 90,                                       #Phi_B default is 90 degrees
           Theta_C = np.degrees(np.arctan(np.sqrt(2))),      #Default upper tetrahedral angle (54.7 degrees)
           Phi_C = 180,                                      #Phi_C default is 180 degrees
           Theta_D = 180-np.degrees(np.arctan(np.sqrt(2))),  #Default lower tetrahedral angle (125.3 degrees)
           Phi_D = 270,                                      #Phi_D default is 270 degrees
           AA=False, BB=False, CC=False, DD=False, Theta=True, Phi=True):
    
    Resolution = Max_Dev_Angle/Increments  #Step size
    Deviation = np.linspace(-Increments*Resolution,Increments*Resolution,2*Increments+1) 
                      
    print("Length of each angle scan: {}".format(len(Deviation)))
    print("Total number of combinations: {}".format(len(Deviation)**(sum([Theta,Phi])*sum([AA,BB,CC,DD]))))

    if len(Deviation)**(sum([Theta,Phi])*sum([AA,BB,CC,DD])) > 1000000:
        
        print("WARNING! MANY COMBINATIONS!")
    
    Theta_A_range = [Theta_A]
    Phi_A_range = [Phi_A]
    Theta_B_range = [Theta_B]
    Phi_B_range = [Phi_B]
    Theta_C_range = [Theta_C]
    Phi_C_range = [Phi_C]
    Theta_D_range = [Theta_D]
    Phi_D_range = [Phi_D]
    
    if AA:
        if Theta:
            Theta_A_range += Deviation
        if Phi:
            Phi_A_range += Deviation
    if BB:
        if Theta:
            Theta_B_range += Deviation
        if Phi:
            Phi_B_range += Deviation
    if CC:
        if Theta:
            Theta_C_range += Deviation
        if Phi:
            Phi_C_range += Deviation
    if DD:
        if Theta:
            Theta_D_range += Deviation
        if Phi:
            Phi_D_range += Deviation
    
    return Theta_A_range, Phi_A_range, Theta_B_range, Phi_B_range, Theta_C_range, Phi_C_range, Theta_D_range, Phi_D_range


def angle_scan(Theta_A_range, Phi_A_range,
               Theta_B_range, Phi_B_range,
               Theta_C_range, Phi_C_range,
               Theta_D_range, Phi_D_range,
               calc_matrix=calc_AA_matrix,
               angle_std=1, V0=np.ones((3,1))):

    AN = nl.inv(calc_matrix())
    V = np.matmul(np.identity(3),V0)
    
    VV=[]
    GG=[]
    
    TA0 = Theta_A_range[((len(Theta_A_range)+1)//2)-1] #Expected Value of Theta_A
    PA0 = Phi_A_range[((len(Phi_A_range)+1)//2)-1]     #Expected Value of Phi_A
    TB0 = Theta_B_range[((len(Theta_B_range)+1)//2)-1] #Expected Value of Theta_B
    PB0 = Phi_B_range[((len(Phi_B_range)+1)//2)-1]     #Expected Value of Phi_B
    TC0 = Theta_C_range[((len(Theta_C_range)+1)//2)-1] #Expected Value of Theta_C
    PC0 = Phi_C_range[((len(Phi_C_range)+1)//2)-1]     #Expected Value of Phi_C
    TD0 = Theta_D_range[((len(Theta_D_range)+1)//2)-1] #Expected Value of Theta_D
    PD0 = Phi_D_range[((len(Phi_D_range)+1)//2)-1]     #Expected Value of Phi_D

    for TA in Theta_A_range:
        for PA in Phi_A_range:
            for TB in Theta_B_range:
                for PB in Phi_B_range:
                    for TC in Theta_C_range:
                        for PC in Phi_C_range:
                            for TD in Theta_D_range:
                                for PD in Phi_D_range:
                                    AP = calc_matrix(Theta_A = TA, Phi_A = PA, Theta_B = TB, Phi_B = PB, 
                                                        Theta_C = TC, Phi_C = PC, Theta_D = TD, Phi_D = PD)
                                    AV = np.matmul(AN,AP)
                                    Vp = np.matmul(AV,np.ones((3,1)))
                                    VV.append(V-Vp)
                                    
                                    GG.append(norm.pdf(TA,TA0,angle_std)*norm.pdf(PA,PA0,angle_std)* \
                                              norm.pdf(TB,TB0,angle_std)*norm.pdf(PB,PB0,angle_std)* \
                                              norm.pdf(TC,TC0,angle_std)*norm.pdf(PC,PC0,angle_std)* \
                                              norm.pdf(TD,TD0,angle_std)*norm.pdf(PD,PD0,angle_std))

    print(len(VV))
    print(len(GG))
    return VV, GG

def sampler(angles,
            TA0=np.degrees(np.arctan(np.sqrt(2))),
            PA0=0,
            TB0=180-np.degrees(np.arctan(np.sqrt(2))),
            PB0=90,
            TC0=np.degrees(np.arctan(np.sqrt(2))),
            PC0=180,
            TD0=180-np.degrees(np.arctan(np.sqrt(2))),
            PD0=270):
        
        TA=angles[0]
        PA=angles[1]
        TB=angles[2]
        PB=angles[3]
        TC=angles[4]
        PC=angles[5]
        TD=angles[6]
        PD=angles[7]
        
        V0=angles[8]
        calc_matrix=angles[9]
        angle_std=angles[10]        
        
        AN = nl.inv(calc_matrix())
        
        V = np.matmul(np.identity(3),V0)
        
        AP = calc_matrix(TA,PA,TB,PB,TC,PC,TD,PD)
        AV = np.matmul(AN,AP)
        Vp = np.matmul(AV,V0)
        VV = V-Vp
        
        GG=norm.pdf(TA,TA0,angle_std)*norm.pdf(PA,PA0,angle_std)* \
        norm.pdf(TB,TB0,angle_std)*norm.pdf(PB,PB0,angle_std)* \
        norm.pdf(TC,TC0,angle_std)*norm.pdf(PC,PC0,angle_std)* \
        norm.pdf(TD,TD0,angle_std)*norm.pdf(PD,PD0,angle_std)
        
        return VV, GG

def angle_scan_parallel(Theta_A_range, Phi_A_range,
               Theta_B_range, Phi_B_range,
               Theta_C_range, Phi_C_range,
               Theta_D_range, Phi_D_range,
               V0=np.ones((3,1)), 
               calc_matrix=calc_AK_matrix,
               angle_std=1):
    
    print('Number of cores available: {}'.format(mpc.cpu_count()))
    
    V0=[V0]
    calc_matrix=[calc_matrix]
    angle_std=[angle_std]
    
    angle_list=list(itt.product(Theta_A_range, Phi_A_range, Theta_B_range, Phi_B_range,
                                Theta_C_range, Phi_C_range, Theta_D_range, Phi_D_range,
                                V0, calc_matrix, angle_std))
    
    pool=mpc.Pool()
    
    res=pool.map(sampler,angle_list)
    
    VV = [res[i][0] for i in range(len(res))]
    GG = [res[i][1] for i in range(len(res))]
    
    print(len(VV))
    print(len(GG))
    
    return VV, GG

